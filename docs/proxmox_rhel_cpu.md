# Proxmox with RHEL 9

## CPU issues

The virtualisation of the CPU for RHEL 9 needs to be set correctly as
RHEL have dropped support for older CPU versions. for RHEL 9 this will
result in a kernel panic for the default Proxmox settings. This
affectsALMAand ROCKY Linux as well.

When setting the CPU in Proxmox there are a number of options. The two
versions that work are "host" or "Nehalem". Host will use the CPU of the
server directly, it is the fastest option but will depend on hardware if migration is used.
For modern identical servers this is not an issue. "Nehalem" is emulated
so the server CPU is not an issue.

The best option in production is "host" and "Nehalem" when VMs may be sent to off site 

These setting are found in the "Create new VM" wizard and can be changed
post install under the VM's hardware section:

## CPU options

![](./img_cpu/xjo2mkdi.png)
![](./img_cpu/3k33oqgf.png)
