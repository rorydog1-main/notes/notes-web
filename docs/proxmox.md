# Proxmox as a VM host for a control system

Physical servers can emulate virtual machines (VMs) at near native speeds. The immediate benefits come from reduced setup times, simplified deployment and ease of scaling. Other benefits are gained from reduced space, cost and energy usage.

## Why Proxmox
Proxmox is a low cost, highly configurable system with a human friendly interface that has been active since 2008.

Under the hood, Proxmox uses opensource existing virtualisation technologies, namely QEMU/KVM. These common technologies form the basis of by other major enterprise virtualisation systems such as OpenStack so are well tested and avoid vendor lock-in.

Proxmox configuration uses sensible default options which was vital when our experience of virtualisation was limited and has an active online community, Proxmox Support Forum .   Large amounts of tricky configuration has already been converted into GUIs that are easy to customise with command line scripts 

Proxmox contains backup and restore tools that can migrate or clone VMs in real time. There is a disk configuration tools, qemu, that allow the easily conversion of VMs drive image to other formats if we need to export or import VMs from other systems or migrate away from Proxmox in the future.

Proxmox offers paid commercial licences or an unsupported free open source licence. 

## Clusters, migration, and backups
Two or more physical hosts can be connected together to act like one. This is called clustering. VMs can be migrated between different physics with no downtime if the network and hardware match. 

### Backup and restore for IOC VM updates vs. migration
Many IOCs connect to hardware or private networks. In this case, migration is broken unless extra external complexly is added. IOCs are simple and the servers robust, several show uptimes of over five years. It is easier to replace servers directly if needed in this case. To do this, an IOC VM was tested by backing up to an external drive via the Proxmox GUI, the host replaced, and the backup reinstalled.

### Cluster, ZFS and migration for NFS servers vs. backups
This live migration acts as a rapid backup and disaster recovery solution for NFS servers. It has also been used to upgrade servers with no VM downtime.

For this to work, the disks on both hosts must be kept in synchronisation. With NFS, a simple rsync between disks will not work and will involve all IOCs being rebooted causing system instability.  

The ZFS file system can synchronise disks across a cluster at a lower level. The synchronisation allows a virtual NFS file server to be migrated to different Proxmox hosts with no downtime. 

## Managing Proxmox
Most operations will be accessible via options from the Proxmox GUI. The deployment process can also be automated with Ansible. The default images are RHEL variants, Centos and Alma. These images have with cloud_init enabled so that the user can change the IP and other network settings from the Proxmox GUI.

 - using ZFS and converting QCOW2 (cloud-init) to ZFS
 - managing Disk images
 - manage an IOC or VM with "deploy_vm"