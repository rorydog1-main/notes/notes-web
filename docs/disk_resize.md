﻿# Read, add, grow or shrink a virtual disk to a VM

## Basic setup

These notes assume:

ZFS file system on host and XFS file system on disks and cloud-init

ZFS is used to treat disk like files. It is called ZVOL. There are several types used for virtualisation, qcow2 is also used by Proxmox and there are others for VMWARE and Virtual box. Most types carry snapshots and clever compression methods. The most basic type is RAW, a binary copy (think dd) of the disk. 

All  type can be converted from one to another using  qemu-img

## Read a ZFS raw disk

If there is a ZFS disk or ZFS snapshot of a disk, this can be mounted and files added or removed as if it were an ordinary drive:

```
# Stop a VM

qm stop 123
# Make a mount
mkdir /mnt/read\_123

# Find a partition to mount
ls /dev/zvol/vm\_cluster/proxmox\_zvols/vm-123-disk-0
/dev/zvol/vm\_cluster/proxmox\_zvols/vm-123-disk-0-part1
/dev/zvol/vm\_cluster/proxmox\_zvols/vm-123-disk-0-part2



# Mount partion 
mount /dev/zvol/vm\_cluster/proxmox\_zvols/vm-123-disk-0-part1 /mnt/read\_123 -t xfs 13
# Do stuff...
cd /mnt/read\_123


# Unmount
cd /
umount /mnt/read\_123
```

## Manually add a disk to a Proxmox VM

There are various cases for this. The main one being system upgrades or updates to the NFS server

**Real use case example: move ext3 to xfs**

Create mountpoints for converting SL7 to Alma9 upgrade. Alma 9 uses XFS and SL7 uses ext3 which is out of date. A new disk be used to update the file system to XFS.

Select the hardware menu of the VM and use “Add” dropdown to select a new disk

![](./disk_img/Aspose.Words.f13afc12-19d8-429f-8bf0-b18f51b5a746.002.png)

Select the ZFS storage, this is am old machine, it will be local-zfs in the future. Set the disk size in Gb and make sure “Discard“ is ticked

![](./disk_img/Aspose.Words.f13afc12-19d8-429f-8bf0-b18f51b5a746.003.png)

This will create a new 1Tb virtual disk. On an Alma Linux VM, the new drive will automatically be detected. It will need to be formatted as XFS1

Open a terminal to the VM and it has been found on /dev/sdc

![](./disk_img/Aspose.Words.f13afc12-19d8-429f-8bf0-b18f51b5a746.004.png)

**Format and mount the blank disk**

Create a new XFS filesystem on the raw disk, partition and make a mountpoint. Add to fstab to mount at boot The files were then moved to the new disk with “rsync”

```

# Make an XFS filesystem. Run as root
makefs.xfs /dev/sdc

# Make a mountpoint
mkdir /mnt/xfs\_update

# Mount and copy with progress indicator (P) and keeping file meta data (A)
mount /dev/sdc /mnt/xfs\_updates
rsync -Pa /home/controls /mnt/xfs\_updates
```
**ZFS is used for virtual disks and XFS for the VM file system inside the disk. This choice was made as ZFS is needed for disk replication and XFS is the default choice for RedHat/Alma disks. Disk snapshots, clones and copies are more robust and faster externally and and faster duplicating a virtual disk than over RSYNC copies with XFS with the VM.**

## Expand a disk

This is done in the hardware section of the Proxmox GUI. Select the size to increase the disk by. Select Cloud-init drive and select “Regenerate”. Stop and restart the VM. Proxmox will do the rest.

## Shrink a disk

Shrinking a disk is more complex as XFS does not allow this directly. This should only happen if the VM can’t easily be remade and is too big for multiple deployments. Normally, it would be faster and safer to rebuild a new VM than do this but there was a case with an oversized VM that needed multiple deployments. This was used to shrink the disk down.

```
15  umount /mnt/test\_mount/
15  umount /mnt/new\_mount/ 17

18 qm start 116
```

**If the kernel has changed:**

Boot into the new smaller VM

DO NOT POWER DOWN:

rsync root@old_vm:/boot /boot

Remake Grub with the correct UUID for the partiton:

grub2-mkconfig -o /boot/grub2/grub.cfg

When OS files are copied like this, the UUID of the partitions will not match and if /boot is changed linux will not boot.  That is why /boot is excluded. If the kernel has not changed then all this is ok

```
# Stop the VM and copy

zfs snapshot vm\_cluster/proxmox\_zvols/vm-115-disk-0@resize\_riverdell
  
# Copy the filesystem to shrink. In this case it is on Partition 1  
# Stop the VM (116 in this case)
mkdir /mnt/test\_mount/ 
# Large disk
mount /dev/zvol/vm\_cluster/proxmox\_zvols/vm-115-disk-0-part1 /mnt/test\_mount/ -t xfs
 
# New small disk 
mount /dev/zvol/vm\_cluster/proxmox\_zvols/vm-116-disk-0-part1 /mnt/new\_mount/ -t xfs
# Copy the root across 
rsync -av --exclude '/boot' /test\_mount/ new\_mount
```