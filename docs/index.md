# Control systems for research
These notes are about control systems used in scientific research environments. The contol system engineer is integrating custom hardware and working in an environment where the design goals may be vauge and can rapidly change.

## Areas of interest are

 - EPICS, a open source distributed control system framework popular with the particle physics community
 - visualization with ANSIBLE
 - data archiving in the EPICS framework, the Archiver Appliance, Proxmox, Promethius, Grafana etc
 - NFS, ZFS and other file system nots
 - lots of Linux